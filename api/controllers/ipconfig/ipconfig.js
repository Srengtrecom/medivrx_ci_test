"use strict";

var os = require('os');
const isWsl = require('is-wsl');

var commands = {
  linux: {
    cmd: 'ifconfig',
    args: ['-a']
  },
  darwin: {
    cmd: 'ifconfig',
    args: ['-a']
  },
  win32: {
    cmd: 'ipconfig',
    args: ['-all']
  }
};

module.exports = function(option, callback) {
  option = option || {};
  const platform = (isWsl ? 'win32' : false) || os.platform();
  const command = commands[platform];

  const { spawn } = require('child_process');
  const activator = spawn(command.cmd, command.args);

  activator.stdout.setEncoding('utf8');
  activator.stdout.on('data', (data) => {
    let str = data.toString().split(/(\r?\n)/g);
    str.forEach((item, index) => {
      if (str[index] !== '\n' && str[index] !== '') {
        if (option.filter) {
          if (item.indexOf(option.filter.description) !== -1 || item.indexOf(option.filter.ip) !== -1) {
            callback = function () {
              return true
            }
          }
        }
      }
    })
  });

  activator.on('close', (code) => {
    console.log(`child process exited with code ${code}`);
  });
}
